#!/usr/bin/python3
"""
Run gRPC logger simple service.

Usage: gloss.py [options]
       gloss.py -h | --help
       gloss.py --version

Options:
  -h, --help                show this help message and exit
  --version                 show program's version number and exit
  -p, --port=PORT           port to be bound [default: 50051]
  -l, --logfile=FILENAME    filename of a log file [default: -]
"""
import json
import logging
import time
import uuid
from concurrent import futures
from distutils.version import LooseVersion, StrictVersion
from typing import Union

import grpc
from docopt import docopt
from fred_api.logger.diagnostics.about_types_pb2 import AboutReply
from fred_api.logger.diagnostics.ping_types_pb2 import PingReply
from fred_api.logger.diagnostics.service_diagnostics_grpc_pb2_grpc import (DiagnosticsServicer,
                                                                           add_DiagnosticsServicer_to_server)
from fred_api.logger.service_logger_grpc_pb2 import (CloseLogEntryReply, CloseSessionReply, CreateLogEntryReply,
                                                     CreateSessionReply, GetLogEntryTypesReply,
                                                     GetObjectReferenceTypesReply, GetResultsReply, GetServicesReply)
from fred_api.logger.service_logger_grpc_pb2_grpc import LoggerServicer, add_LoggerServicer_to_server
from frgal import GrpcDecoder

__version__ = '4.0.0'


class SimpleLoggerServicer(LoggerServicer):
    """Simple implementation of the logger service."""

    def __init__(self, *args, **kwargs):
        """Initialize servicer."""
        super().__init__(*args, **kwargs)
        self.decoder = GrpcDecoder()

    def _make_log(self, msg, request, *args):
        logging.info(msg, json.dumps(self.decoder.decode(request)), *args)

    def create_log_entry(self, request, context):
        """Create a new log entry."""
        response_id = str(uuid.uuid4())
        self._make_log("create_log_entry: %s -> %s", request, response_id)

        response = CreateLogEntryReply()
        response.data.log_entry_id.value = response_id
        return response

    def close_log_entry(self, request, context):
        """Close a log entry."""
        self._make_log("close_log_entry: %s", request)

        response = CloseLogEntryReply()
        return response

    def create_session(self, request, context):
        """Crate a new session."""
        session_id = str(uuid.uuid4())
        self._make_log("create_session: %s -> %s", request, session_id)

        response = CreateSessionReply()
        response.data.session_id.value = session_id
        return response

    def close_session(self, request, context):
        """Close a session."""
        self._make_log("close_session: %s", request)

        response = CloseSessionReply()
        return response

    def get_log_entry_types(self, request, context):
        """Return available log entry types."""
        self._make_log("get_log_entry_types: %s", request)

        response = GetLogEntryTypesReply()
        return response

    def get_services(self, request, context):
        """Return available services."""
        self._make_log("get_services: %s", request)

        response = GetServicesReply()
        return response

    def get_results(self, request, context):
        """Return available results."""
        self._make_log("get_results: %s", request)

        response = GetResultsReply()
        return response

    def get_object_reference_types(self, request, context):
        """Return available object reference types."""
        self._make_log("get_object_reference_types: %s", request)

        response = GetObjectReferenceTypesReply()
        return response


class SimpleDiagnosticsServicer(DiagnosticsServicer):
    """Simple implementation of the logger diagnostics servicer."""

    def about(self, request, context):
        """Return informations about version of API and server."""
        response = AboutReply()
        response.data.server_version.full = __version__
        try:
            version = StrictVersion(__version__)  # type: Union[StrictVersion, LooseVersion]
        except ValueError:
            version = LooseVersion(__version__)
        response.data.server_version.major = version.version[0]
        response.data.server_version.minor = version.version[1]
        response.data.server_version.patch = version.version[2]
        if 'rc' in version.version:
            response.data.server_version.rc = version.version[version.version.index('rc') + 1]
        return response

    def ping(self, request, context):
        """Check server accessibility."""
        return PingReply()


def main():
    """Start the service."""
    options = docopt(__doc__, version=__version__)

    if options['--logfile'] != '-':
        logging.basicConfig(level=logging.INFO, filename=options['--logfile'])
    else:
        logging.basicConfig(level=logging.INFO)

    server = grpc.server(futures.ThreadPoolExecutor())
    add_LoggerServicer_to_server(SimpleLoggerServicer(), server)
    add_DiagnosticsServicer_to_server(SimpleDiagnosticsServicer(), server)
    server.add_insecure_port('[::]:{port}'.format(port=options['--port']))
    server.start()
    # Sadly this is the recommended way to keep the server running.
    try:
        while True:
            time.sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    main()
