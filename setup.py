#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Setup script for gloss."""
import os

from setuptools import find_packages, setup


def readme():
    """Return content of README file."""
    with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as f:
        return f.read()


INSTALL_REQUIRES = open('requirements.txt').read().splitlines()
EXTRAS_REQUIRE = {'quality': ['isort', 'flake8', 'pydocstyle', 'mypy']}
VERSION = '4.0.0'


setup(name='gloss',
      version=VERSION,
      description='gRPC logger simple service',
      long_description=readme(),
      author='Vlastimil Zíma',
      author_email='vlastimil.zima@nic.cz',
      py_modules=['gloss'],
      python_requires='>=3.5',
      install_requires=INSTALL_REQUIRES,
      extras_require=EXTRAS_REQUIRE,
      entry_points={'console_scripts': ['gloss = gloss:main']},
      classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries',
        'Topic :: Software Development :: Libraries :: Python Modules',
      ])
